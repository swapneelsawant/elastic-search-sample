package com.ateam.elastic.exception;

public class ESRuntimeException extends RuntimeException {
    public ESRuntimeException() {
    }

    public ESRuntimeException(String message) {
        super ( message );
    }

    public ESRuntimeException(String message, Throwable cause) {
        super ( message, cause );
    }

    public ESRuntimeException(Throwable cause) {
        super ( cause );
    }

    public ESRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super ( message, cause, enableSuppression, writableStackTrace );
    }
}
