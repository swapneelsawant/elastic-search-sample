package com.ateam.elastic.rest.search;

import com.ateam.elastic.rest.client.ESRestClient;
import com.ateam.elastic.rest.client.high.ESRestClientImpl;
import com.ateam.elastic.rest.index.ESIndexingApi;
import com.ateam.elastic.rest.index.ESIndexingImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Cancellable;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.SuggestionBuilder;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * QueryBuilders.boolQuery()
 * .must(QueryBuilders.termQuery("content", "test1"))
 * .must(QueryBuilders.termQuery("content", "test4"))
 * .mustNot(QueryBuilders.termQuery("content", "test2"))
 * .should(QueryBuilders.termQuery("content", "test3"))
 * .filter(QueryBuilders.termQuery("content", "test5"));
 * searchSourceBuilder.profile(true);
 * https://www.elastic.co/guide/en/elasticsearch/client/java-api/current/java-compound-queries.html
 * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/master/java-rest-high-search.html
 */
public class ESSearchingImpl implements ESSearchingApi {

    private static final int FROM = 0;
    private static final int SIZE = 10;
    private static final int TIME_OUT_IN_SEC = 60;
    private static final boolean FETCH_SOURCE = true;

    private static final Logger LOGGER = Logger.getLogger ( ESSearchingImpl.class );

    private ESRestClient<RestHighLevelClient> esRestClient;

    public ESSearchingImpl(ESRestClient<RestHighLevelClient> esRestClient) {
        this.esRestClient = esRestClient;
    }

    @Override
    public SearchResponse search(String indexName, QueryBuilder queryBuilder) throws IOException {
        return search ( indexName, null, FROM, SIZE, null, null, null, queryBuilder );
    }

    @Override
    public SearchResponse search(String indexName, int from, int size, QueryBuilder queryBuilder) throws IOException {
        return search ( indexName, null, from, size, null, null, null, queryBuilder );
    }

    @Override
    public SearchResponse search(String indexName, String routing, int from, int size, QueryBuilder queryBuilder) throws IOException {
        return search ( indexName, routing, from, size, null, null, null, queryBuilder );
    }

    @Override
    public SearchResponse search(String indexName, String routing, int from, int size, List<FieldSortBuilder> fieldSortBuilders, String[] includeFields, String[] excludeFields, QueryBuilder queryBuilder) throws IOException {

        SearchRequest searchRequest = getSearchRequest ( indexName, routing, from, size, fieldSortBuilders, includeFields, excludeFields, queryBuilder );
        LOGGER.info ( searchRequest.toString () );

        SearchResponse searchResponse = this.esRestClient.getRestClient ().search ( searchRequest, RequestOptions.DEFAULT );
        RestStatus status = searchResponse.status ();
        TimeValue took = searchResponse.getTook ();
        LOGGER.info ( "status : " + status.name () + " took : " + took.getMillis () + "ms" );
        return searchResponse;
    }


    @Override
    public Cancellable searchAsync(String indexName, QueryBuilder queryBuilder, ActionListener<SearchResponse> listener) {
        return searchAsync ( indexName, null, FROM, SIZE, null, null, null, queryBuilder, listener );
    }

    @Override
    public Cancellable searchAsync(String indexName, int from, int size, QueryBuilder queryBuilder, ActionListener<SearchResponse> listener) {
        return searchAsync ( indexName, null, from, size, null, null, null, queryBuilder, listener );
    }

    @Override
    public Cancellable searchAsync(String indexName, String routing, int from, int size, QueryBuilder queryBuilder, ActionListener<SearchResponse> listener) {
        return searchAsync ( indexName, routing, from, size, null, null, null, queryBuilder, listener );
    }

    @Override
    public Cancellable searchAsync(String indexName, String routing, int from, int size, List<FieldSortBuilder> fieldSortBuilders, String[] includeFields, String[] excludeFields, QueryBuilder queryBuilder, ActionListener<SearchResponse> listener) {
        SearchRequest searchRequest = getSearchRequest ( indexName, routing, from, size, fieldSortBuilders, includeFields, excludeFields, queryBuilder );
        return this.esRestClient.getRestClient ().searchAsync ( searchRequest, RequestOptions.DEFAULT, listener );
    }

    private SearchRequest getSearchRequest(String indexName, String routing, int from, int size, List<FieldSortBuilder> fieldSortBuilders, String[] includeFields, String[] excludeFields, QueryBuilder queryBuilder) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder ();
        searchSourceBuilder.from ( from );
        searchSourceBuilder.size ( size );
        searchSourceBuilder.timeout ( new TimeValue ( TIME_OUT_IN_SEC, TimeUnit.SECONDS ) );
        searchSourceBuilder.fetchSource ( FETCH_SOURCE );
        if (CollectionUtils.isNotEmpty ( fieldSortBuilders ))
            fieldSortBuilders.forEach ( searchSourceBuilder::sort );
        searchSourceBuilder.fetchSource ( includeFields, excludeFields );
        searchSourceBuilder.query ( queryBuilder );

        SearchRequest searchRequest = new SearchRequest ( indexName );
        searchRequest.source ( searchSourceBuilder );
        searchRequest.routing ( routing );
        return searchRequest;
    }

    //TODO
    private void aggregation() {
        //https://www.elastic.co/guide/en/elasticsearch/client/java-rest/current/java-rest-high-aggregation-builders.html
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder ();
        TermsAggregationBuilder aggregation = AggregationBuilders.terms ( "by_company" )
                .field ( "company.keyword" );
        aggregation.subAggregation ( AggregationBuilders.avg ( "average_age" )
                .field ( "age" ) );
        searchSourceBuilder.aggregation ( aggregation );
    }

    //TODO
    private void suggestion() throws IOException {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder ();
        SuggestionBuilder termSuggestionBuilder =
                SuggestBuilders.termSuggestion ( "user" ).text ( "kmich" );
        SuggestBuilder suggestBuilder = new SuggestBuilder ();
        suggestBuilder.addSuggestion ( "suggest_user", termSuggestionBuilder );
        searchSourceBuilder.suggest ( suggestBuilder );

        SearchRequest sr = new SearchRequest ( "posts" );
        sr.source ( searchSourceBuilder );
        ESRestClientImpl esRestClient = new ESRestClientImpl ();
        esRestClient.initRestClient ();
        ESIndexingApi esIndexingApi = new ESIndexingImpl ( esRestClient );
        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        SearchResponse searchResponse = highLevelClient.search ( sr, RequestOptions.DEFAULT );
        System.out.println ( searchResponse.getSuggest ().toString () );
        esRestClient.shutdownRestClient ();
    }

}
