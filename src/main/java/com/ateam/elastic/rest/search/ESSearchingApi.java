package com.ateam.elastic.rest.search;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Cancellable;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;

import java.io.IOException;
import java.util.List;

public interface ESSearchingApi {
    SearchResponse search(String indexName, QueryBuilder queryBuilder) throws IOException;

    SearchResponse search(String indexName, int from, int size, QueryBuilder queryBuilder) throws IOException;

    SearchResponse search(String indexName, String routing, int from, int size, QueryBuilder queryBuilder) throws IOException;

    SearchResponse search(String indexName, String routing, int from, int size, List<FieldSortBuilder> fieldSortBuilders, String[] includeFields, String[] excludeFields, QueryBuilder queryBuilder) throws IOException;

    Cancellable searchAsync(String indexName, QueryBuilder queryBuilder, ActionListener<SearchResponse> listener) throws IOException;

    Cancellable searchAsync(String indexName, int from, int size, QueryBuilder queryBuilder, ActionListener<SearchResponse> listener) throws IOException;

    Cancellable searchAsync(String indexName, String routing, int from, int size, QueryBuilder queryBuilder, ActionListener<SearchResponse> listener) throws IOException;

    Cancellable searchAsync(String indexName, String routing, int from, int size, List<FieldSortBuilder> fieldSortBuilders, String[] includeFields, String[] excludeFields, QueryBuilder queryBuilder, ActionListener<SearchResponse> listener) throws IOException;
}
