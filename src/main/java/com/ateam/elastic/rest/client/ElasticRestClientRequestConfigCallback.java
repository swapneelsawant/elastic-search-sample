package com.ateam.elastic.rest.client;

import org.apache.http.client.config.RequestConfig;
import org.elasticsearch.client.RestClientBuilder;

public class ElasticRestClientRequestConfigCallback implements RestClientBuilder.RequestConfigCallback {
    @Override
    public RequestConfig.Builder customizeRequestConfig(
            RequestConfig.Builder requestConfigBuilder) {
        return requestConfigBuilder.setSocketTimeout ( 10000 );
    }
}
