package com.ateam.elastic.rest.client;

import org.elasticsearch.client.Node;
import org.elasticsearch.client.RestClient;

public class ElasticRestClientFailureListener extends RestClient.FailureListener {

    @Override
    public void onFailure(Node node) {
        System.out.println ( "On failue " + node.getName () );
    }
}
