package com.ateam.elastic.rest.client.low;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.client.Response;

public class DefaultActionListener implements ActionListener<Response> {

    @Override
    public void onResponse(Response response) {
        System.out.println ( response.getStatusLine () );
    }

    @Override
    public void onFailure(Exception e) {
        e.printStackTrace ();
    }
}
