package com.ateam.elastic.rest.client.high;

import com.ateam.elastic.rest.client.ESRestClient;
import com.ateam.elastic.rest.client.ElasticRestClientFailureListener;
import com.ateam.elastic.rest.client.ElasticRestClientRequestConfigCallback;
import org.apache.http.HttpHost;
import org.elasticsearch.client.NodeSelector;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import javax.annotation.PostConstruct;
import java.io.IOException;

public class ESRestClientImpl implements ESRestClient<RestHighLevelClient> {

    private RestHighLevelClient client;

    @PostConstruct
    public void initRestClient() {
        client = new RestHighLevelClient (
                RestClient.builder (
                        new HttpHost ( "localhost", 9200, "http" ) )
                        .setDefaultHeaders ( getHeader () )
                        .setFailureListener ( new ElasticRestClientFailureListener () )
                        .setNodeSelector ( NodeSelector.SKIP_DEDICATED_MASTERS )
                        .setRequestConfigCallback ( new ElasticRestClientRequestConfigCallback () )
        );
    }

    @Override
    public RestHighLevelClient getRestClient() {
        return client;
    }

    @Override
    public void shutdownRestClient() throws IOException {
        client.close ();
    }
}
