package com.ateam.elastic.rest.client.low;

import com.ateam.elastic.rest.client.ESRestClient;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.client.*;

import java.io.IOException;
import java.util.Map;

public class ESRequestFrameWork {

    private static final RequestOptions COMMON_OPTIONS;

    static {
        String TOKEN = "";
        RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder ();
        builder.addHeader ( "Authorization", "Bearer " + TOKEN );
        builder.setHttpAsyncResponseConsumerFactory (
                new HttpAsyncResponseConsumerFactory
                        .HeapBufferedResponseConsumerFactory ( 30 * 1024 * 1024 * 1024 ) );
        COMMON_OPTIONS = builder.build ();
    }

    private ESRestClient<RestClient> esRestClient;

    public ESRequestFrameWork(ESRestClient<RestClient> esRestClient) {
        this.esRestClient = esRestClient;
    }

    public Response performRequest(String method, String endpoint) throws IOException {
        return performRequest ( method, endpoint, null, null );
    }

    public Response performRequest(String method, String endpoint, Map<String, String> parameters) throws IOException {
        return performRequest ( method, endpoint, parameters, null );
    }

    public Response performRequest(String method, String endpoint, String jsonBodyContent) throws IOException {
        return performRequest ( method, endpoint, null, jsonBodyContent );
    }

    public Response performRequest(String method, String endpoint, Map<String, String> parameters, String jsonBodyContent) throws IOException {
        Request request = getRequest ( method, endpoint, parameters, jsonBodyContent );
        return esRestClient.getRestClient ().performRequest ( request );
    }

    public Cancellable performRequestAsync(String method, String endpoint, ResponseListener responseListener) {
        return performRequestAsync ( method, endpoint, null, null, responseListener );
    }

    public Cancellable performRequestAsync(String method, String endpoint, Map<String, String> parameters, ResponseListener responseListener) {
        return performRequestAsync ( method, endpoint, parameters, null, responseListener );
    }

    public Cancellable performRequestAsync(String method, String endpoint, String jsonBodyContent, ResponseListener responseListener) {
        return performRequestAsync ( method, endpoint, null, jsonBodyContent, responseListener );
    }

    public Cancellable performRequestAsync(String method, String endpoint, Map<String, String> parameters, String jsonBodyContent, ResponseListener responseListener) {
        Request request = getRequest ( method, endpoint, parameters, jsonBodyContent );
        return esRestClient.getRestClient ().performRequestAsync ( request, responseListener );
    }

    private Request getRequest(String method, String endpoint, Map<String, String> parameters, String jsonBodyContent) {
        Request request = new Request (
                method,
                endpoint );
        if (MapUtils.isNotEmpty ( parameters )) {
            parameters.forEach ( (k, v) -> request.addParameter ( k, v ) );
        }

        if (StringUtils.isNotEmpty ( jsonBodyContent ))
            request.setJsonEntity ( jsonBodyContent );
        //request.setOptions(COMMON_OPTIONS);
        return request;
    }

    public static class ESHttpReponse {
        /*


         */
    }

    public static class LoggerResponseListener implements ResponseListener {

        @Override
        public void onSuccess(Response response) {
            System.out.println ( response.getEntity ().getContentLength () );
        }

        @Override
        public void onFailure(Exception exception) {
            exception.printStackTrace ();
        }
    }
}
