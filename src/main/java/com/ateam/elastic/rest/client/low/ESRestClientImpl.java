package com.ateam.elastic.rest.client.low;

import com.ateam.elastic.rest.client.ESRestClient;
import com.ateam.elastic.rest.client.ElasticRestClientFailureListener;
import com.ateam.elastic.rest.client.ElasticRestClientRequestConfigCallback;
import org.apache.http.HttpHost;
import org.elasticsearch.client.NodeSelector;
import org.elasticsearch.client.RestClient;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * High api uses the low level api
 */
public class ESRestClientImpl implements ESRestClient<RestClient> {

    private RestClient restClient;

    @Override
    @PostConstruct
    public void initRestClient() {
        this.restClient = RestClient.builder (
                new HttpHost ( "localhost", 9200, "http" ) )
                .setDefaultHeaders ( getHeader () )
                .setFailureListener ( new ElasticRestClientFailureListener () )
                .setNodeSelector ( NodeSelector.SKIP_DEDICATED_MASTERS )
                .setRequestConfigCallback ( new ElasticRestClientRequestConfigCallback () )
                .build ();
    }

    @Override
    public RestClient getRestClient() {
        return restClient;
    }

    @Override
    public void shutdownRestClient() throws IOException {
        restClient.close ();
    }

}
