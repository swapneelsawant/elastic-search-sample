package com.ateam.elastic.rest.client;

import org.apache.http.HttpHost;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.client.RestClientBuilder;

/**
 * Set a callback that allows to modify the http client configuration (e.g. encrypted communication over ssl, or
 * anything that the org.apache.http.impl.nio.client.HttpAsyncClientBuilder allows to set)
 * builder.setHttpClientConfigCallback(new ElasticRestClientHttpClientConfigCallback())
 */
public class ElasticRestClientHttpClientConfigCallback implements RestClientBuilder.HttpClientConfigCallback {
    @Override
    public HttpAsyncClientBuilder customizeHttpClient(
            HttpAsyncClientBuilder httpClientBuilder) {
        return httpClientBuilder.setProxy (
                new HttpHost ( "proxy", 9000, "http" ) );
    }
}
