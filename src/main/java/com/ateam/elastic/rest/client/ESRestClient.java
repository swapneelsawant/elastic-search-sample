package com.ateam.elastic.rest.client;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import javax.annotation.PostConstruct;
import java.io.IOException;

public interface ESRestClient<T> {
    @PostConstruct
    void initRestClient();

    T getRestClient();

    void shutdownRestClient() throws IOException;

    default Header[] getHeader() {
        return new Header[]{new BasicHeader ( "Content-Type", "application/json" )};
    }
}
