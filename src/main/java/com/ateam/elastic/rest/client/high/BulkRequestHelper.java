package com.ateam.elastic.rest.client.high;

import com.ateam.elastic.rest.ESRequestApi;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.action.support.replication.ReplicatedWriteRequest;
import org.elasticsearch.common.unit.TimeValue;

import java.util.List;

public class BulkRequestHelper {

    private static final long TIME_OUT_IN_MIN_BULK = 2L;

    public static <T, R extends ReplicatedWriteRequest> BulkRequest getBulkRequest(String indexName, List<String> ids, String routing, String pipeline, List<T> tList, ESRequestApi<T, R> esRequestApi) {

        if (ids.size () != tList.size ())
            throw new IllegalArgumentException ( "ids.size != tList.size" );

        BulkRequest bulkRequest = new BulkRequest ();
        for (int i = 0; i < ids.size (); i++) {
            T t = tList.get ( i );
            String id = ids.get ( i );
            ReplicatedWriteRequest replicatedWriteRequest = esRequestApi.getRequest ( indexName, id, routing, pipeline, t );
            if (replicatedWriteRequest instanceof IndexRequest)
                bulkRequest.add ( (IndexRequest) replicatedWriteRequest );
            else if (replicatedWriteRequest instanceof DeleteRequest)
                bulkRequest.add ( (DeleteRequest) replicatedWriteRequest );
            else throw new IllegalArgumentException ( replicatedWriteRequest.getClass () + "is not supported" );
        }
        bulkRequest.routing ( routing );
        bulkRequest.timeout ( TimeValue.timeValueMinutes ( TIME_OUT_IN_MIN_BULK ) );
        bulkRequest.setRefreshPolicy ( WriteRequest.RefreshPolicy.WAIT_UNTIL );
        //https://www.elastic.co/guide/en/elasticsearch/reference/master/ingest-apis.html
        bulkRequest.pipeline ( pipeline );
        return bulkRequest;
    }
}
