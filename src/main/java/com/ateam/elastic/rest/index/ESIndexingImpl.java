package com.ateam.elastic.rest.index;

import com.ateam.elastic.rest.client.ESRestClient;
import com.ateam.elastic.rest.client.high.BulkRequestHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Cancellable;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;
import java.util.List;

public class ESIndexingImpl<T> implements ESIndexingApi<T> {

    private ESRestClient<RestHighLevelClient> esRestClient;
    private ESIndexRequestImpl<T> esIndexRequestImpl;

    public ESIndexingImpl(ESRestClient<RestHighLevelClient> esRestClient) {
        this.esRestClient = esRestClient;
        this.esIndexRequestImpl = new ESIndexRequestImpl<> ();
    }

    public ESIndexingImpl(ESRestClient<RestHighLevelClient> esRestClient, ObjectMapper mapper) {
        this.esRestClient = esRestClient;
        this.esIndexRequestImpl = new ESIndexRequestImpl<> ( mapper );
    }

    @Override
    public IndexResponse index(String indexName, String id, T t) throws IOException {
        return index ( indexName, id, null, null, t );
    }

    @Override
    public IndexResponse index(String indexName, String id, T t, String routing) throws IOException {
        return index ( indexName, id, routing, null, t );
    }

    @Override
    public IndexResponse index(String indexName, String id, String pipeline, T t) throws IOException {
        return index ( indexName, id, null, pipeline, t );
    }

    @Override
    public IndexResponse index(String indexName, String id, String routing, String pipeline, T t) throws IOException {
        IndexRequest request = esIndexRequestImpl.getRequest ( indexName, id, routing, pipeline, t );

        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        return highLevelClient.index ( request, RequestOptions.DEFAULT );
    }

    @Override
    public BulkResponse indexBulk(String indexName, List<String> ids, List<T> tList) throws IOException {
        return indexBulk ( indexName, ids, null, null, tList );
    }

    @Override
    public BulkResponse indexBulk(String indexName, List<String> ids, List<T> tList, String routing) throws IOException {
        return indexBulk ( indexName, ids, routing, null, tList );
    }

    @Override
    public BulkResponse indexBulk(String indexName, List<String> ids, String pipeline, List<T> tList) throws IOException {
        return indexBulk ( indexName, ids, null, pipeline, tList );
    }

    @Override
    public BulkResponse indexBulk(String indexName, List<String> ids, String routing, String pipeline, List<T> tList) throws IOException {
        BulkRequest bulkRequest = BulkRequestHelper.getBulkRequest ( indexName, ids, routing, pipeline, tList, esIndexRequestImpl );
        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        return highLevelClient.bulk ( bulkRequest, RequestOptions.DEFAULT );
    }


    @Override
    public Cancellable indexAsync(String indexName, String id, T t, ActionListener<IndexResponse> listener) {
        return indexAsync ( indexName, id, null, null, t, listener );
    }

    @Override
    public Cancellable indexAsync(String indexName, String id, T t, String routing, ActionListener<IndexResponse> listener) {
        return indexAsync ( indexName, id, routing, null, t, listener );
    }

    @Override
    public Cancellable indexAsync(String indexName, String id, String pipeline, T t, ActionListener<IndexResponse> listener) {
        return indexAsync ( indexName, id, null, pipeline, t, listener );
    }

    @Override
    public Cancellable indexAsync(String indexName, String id, String routing, String pipeline, T t, ActionListener<IndexResponse> listener) {
        IndexRequest request = esIndexRequestImpl.getRequest ( indexName, id, routing, pipeline, t );
        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        return highLevelClient.indexAsync ( request, RequestOptions.DEFAULT, listener );
    }

    @Override
    public Cancellable indexBulkAsync(String indexName, List<String> ids, List<T> tList, ActionListener<BulkResponse> listener) {
        return indexBulkAsync ( indexName, ids, null, null, tList, listener );
    }

    @Override
    public Cancellable indexBulkAsync(String indexName, List<String> ids, List<T> tList, String routing, ActionListener<BulkResponse> listener) {
        return indexBulkAsync ( indexName, ids, routing, null, tList, listener );
    }

    @Override
    public Cancellable indexBulkAsync(String indexName, List<String> ids, String pipeline, List<T> tList, ActionListener<BulkResponse> listener) {
        return indexBulkAsync ( indexName, ids, null, pipeline, tList, listener );
    }

    @Override
    public Cancellable indexBulkAsync(String indexName, List<String> ids, String routing, String pipeline, List<T> tList, ActionListener<BulkResponse> listener) {
        BulkRequest bulkRequest = BulkRequestHelper.getBulkRequest ( indexName, ids, routing, pipeline, tList, esIndexRequestImpl );
        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        return highLevelClient.bulkAsync ( bulkRequest, RequestOptions.DEFAULT, listener );
    }


    /** how to handle response
     *
     * String index = indexResponse.getIndex();
     * String id = indexResponse.getId();
     * if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
     *
     * } else if (indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
     *
     * }
     * ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
     * if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
     *
     * }
     * if (shardInfo.getFailed() > 0) {
     *     for (ReplicationResponse.ShardInfo.Failure failure :
     *             shardInfo.getFailures()) {
     *         String reason = failure.reason();
     *     }
     * }
     */

}
