package com.ateam.elastic.rest.index;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Cancellable;

import java.io.IOException;
import java.util.List;

public interface ESIndexingApi<T> {
    IndexResponse index(String indexName, String id, T t) throws IOException;

    IndexResponse index(String indexName, String id, T t, String routing) throws IOException;

    IndexResponse index(String indexName, String id, String pipeline, T t) throws IOException;

    IndexResponse index(String indexName, String id, String routing, String pipeline, T t) throws IOException;

    BulkResponse indexBulk(String indexName, List<String> ids, List<T> tList) throws IOException;

    BulkResponse indexBulk(String indexName, List<String> ids, List<T> tList, String routing) throws IOException;

    BulkResponse indexBulk(String indexName, List<String> ids, String pipeline, List<T> tList) throws IOException;

    BulkResponse indexBulk(String indexName, List<String> ids, String routing, String pipeline, List<T> tList) throws IOException;

    Cancellable indexAsync(String indexName, String id, T t, ActionListener<IndexResponse> listener) throws IOException;

    Cancellable indexAsync(String indexName, String id, T t, String routing, ActionListener<IndexResponse> listener) throws IOException;

    Cancellable indexAsync(String indexName, String id, String pipeline, T t, ActionListener<IndexResponse> listener) throws IOException;

    Cancellable indexAsync(String indexName, String id, String routing, String pipeline, T t, ActionListener<IndexResponse> listener);

    Cancellable indexBulkAsync(String indexName, List<String> ids, List<T> tList, ActionListener<BulkResponse> listener) throws IOException;

    Cancellable indexBulkAsync(String indexName, List<String> ids, List<T> tList, String routing, ActionListener<BulkResponse> listener) throws IOException;

    Cancellable indexBulkAsync(String indexName, List<String> ids, String pipeline, List<T> tList, ActionListener<BulkResponse> listener) throws IOException;

    Cancellable indexBulkAsync(String indexName, List<String> ids, String routing, String pipeline, List<T> tList, ActionListener<BulkResponse> listener);
}
