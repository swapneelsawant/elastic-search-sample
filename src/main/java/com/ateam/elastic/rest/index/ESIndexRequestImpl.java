package com.ateam.elastic.rest.index;

import com.ateam.elastic.exception.ESRuntimeException;
import com.ateam.elastic.rest.ESRequestApi;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;

public class ESIndexRequestImpl<T> implements ESRequestApi<T, IndexRequest> {

    private static final long TIME_OUT_IN_SEC = 1L;

    private ObjectMapper mapper;

    public ESIndexRequestImpl() {
        mapper = new ObjectMapper ();
        mapper.disable ( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS );
        mapper.setDateFormat ( new StdDateFormat ().withColonInTimeZone ( true ) );
    }

    public ESIndexRequestImpl(ObjectMapper mapper) {
        this.mapper = mapper;
    }


    @Override
    public IndexRequest getRequest(String indexName, String id, String routing, String pipeline, T t) {
        IndexRequest request = new IndexRequest ( indexName );
        request.id ( id );
        String jsonString = getJson ( t );

        request.source ( jsonString, XContentType.JSON );
        request.routing ( routing );
        request.timeout ( TimeValue.timeValueSeconds ( TIME_OUT_IN_SEC ) );
        request.setRefreshPolicy ( WriteRequest.RefreshPolicy.WAIT_UNTIL );
        //request.versionType( VersionType.EXTERNAL);
        request.opType ( DocWriteRequest.OpType.INDEX );

        //https://www.elastic.co/guide/en/elasticsearch/reference/master/ingest-apis.html
        request.setPipeline ( pipeline );
        return request;
    }

    private String getJson(T t) {
        try {
            return mapper.writeValueAsString ( t );
        } catch (JsonProcessingException e) {
            throw new ESRuntimeException ( e );
        }
//        Object o = mapper.readValue(json, Object.class);
    }
}
