package com.ateam.elastic.rest.delete;

import com.ateam.elastic.rest.ESRequestApi;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.common.unit.TimeValue;

public class ESDeleteRequestImpl<T> implements ESRequestApi<T, DeleteRequest> {

    private static final long TIME_OUT_IN_SEC = 1L;

    public ESDeleteRequestImpl() {
    }

    @Override
    public DeleteRequest getRequest(String indexName, String id, String routing, String pipeline, T t) {
        DeleteRequest request = new DeleteRequest ( indexName );
        request.id ( id );
        request.routing ( routing );
        request.timeout ( TimeValue.timeValueSeconds ( TIME_OUT_IN_SEC ) );
        request.setRefreshPolicy ( WriteRequest.RefreshPolicy.WAIT_UNTIL );
        //request.versionType( VersionType.EXTERNAL);
        return request;
    }
}
