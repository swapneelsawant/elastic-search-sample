package com.ateam.elastic.rest.delete;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.client.Cancellable;

import java.io.IOException;
import java.util.List;

public interface ESDeletingApi<T> {
    DeleteResponse delete(String indexName, String id, T t) throws IOException;

    DeleteResponse delete(String indexName, String id, T t, String routing) throws IOException;

    DeleteResponse delete(String indexName, String id, String pipeline, T t) throws IOException;

    DeleteResponse delete(String indexName, String id, String routing, String pipeline, T t) throws IOException;

    BulkResponse deleteBulk(String indexName, List<String> ids, List<T> tList) throws IOException;

    BulkResponse deleteBulk(String indexName, List<String> ids, List<T> tList, String routing) throws IOException;

    BulkResponse deleteBulk(String indexName, List<String> ids, String pipeline, List<T> tList) throws IOException;

    BulkResponse deleteBulk(String indexName, List<String> ids, String routing, String pipeline, List<T> tList) throws IOException;

    Cancellable deleteAsync(String indexName, String id, T t, ActionListener<DeleteResponse> listener) throws IOException;

    Cancellable deleteAsync(String indexName, String id, T t, String routing, ActionListener<DeleteResponse> listener) throws IOException;

    Cancellable deleteAsync(String indexName, String id, String pipeline, T t, ActionListener<DeleteResponse> listener) throws IOException;

    Cancellable deleteAsync(String indexName, String id, String routing, String pipeline, T t, ActionListener<DeleteResponse> listener);

    Cancellable deleteBulkAsync(String indexName, List<String> ids, List<T> tList, ActionListener<BulkResponse> listener) throws IOException;

    Cancellable deleteBulkAsync(String indexName, List<String> ids, List<T> tList, String routing, ActionListener<BulkResponse> listener) throws IOException;

    Cancellable deleteBulkAsync(String indexName, List<String> ids, String pipeline, List<T> tList, ActionListener<BulkResponse> listener) throws IOException;

    Cancellable deleteBulkAsync(String indexName, List<String> ids, String routing, String pipeline, List<T> tList, ActionListener<BulkResponse> listener);
}
