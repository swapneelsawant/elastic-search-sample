package com.ateam.elastic.rest.delete;

import com.ateam.elastic.rest.ESRequestApi;
import com.ateam.elastic.rest.client.ESRestClient;
import com.ateam.elastic.rest.client.high.BulkRequestHelper;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.client.Cancellable;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;
import java.util.List;

public class ESDeletingImpl<T> implements ESDeletingApi<T> {

    private ESRestClient<RestHighLevelClient> esRestClient;
    private ESRequestApi<T, DeleteRequest> esDeleteRequestImpl;

    public ESDeletingImpl(ESRestClient<RestHighLevelClient> esRestClient) {
        this.esRestClient = esRestClient;
        this.esDeleteRequestImpl = new ESDeleteRequestImpl<> ();
    }

    @Override
    public DeleteResponse delete(String indexName, String id, T t) throws IOException {
        return delete ( indexName, id, null, null, t );
    }

    @Override
    public DeleteResponse delete(String indexName, String id, T t, String routing) throws IOException {
        return delete ( indexName, id, routing, null, t );
    }

    @Override
    public DeleteResponse delete(String indexName, String id, String pipeline, T t) throws IOException {
        return delete ( indexName, id, null, pipeline, t );
    }

    @Override
    public DeleteResponse delete(String indexName, String id, String routing, String pipeline, T t) throws IOException {
        DeleteRequest request = esDeleteRequestImpl.getRequest ( indexName, id, routing, pipeline, t );

        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        return highLevelClient.delete ( request, RequestOptions.DEFAULT );
    }

    @Override
    public BulkResponse deleteBulk(String indexName, List<String> ids, List<T> tList) throws IOException {
        return deleteBulk ( indexName, ids, null, null, tList );
    }

    @Override
    public BulkResponse deleteBulk(String indexName, List<String> ids, List<T> tList, String routing) throws IOException {
        return deleteBulk ( indexName, ids, routing, null, tList );
    }

    @Override
    public BulkResponse deleteBulk(String indexName, List<String> ids, String pipeline, List<T> tList) throws IOException {
        return deleteBulk ( indexName, ids, null, pipeline, tList );
    }

    @Override
    public BulkResponse deleteBulk(String indexName, List<String> ids, String routing, String pipeline, List<T> tList) throws IOException {
        BulkRequest bulkRequest = BulkRequestHelper.getBulkRequest ( indexName, ids, routing, pipeline, tList, esDeleteRequestImpl );
        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        return highLevelClient.bulk ( bulkRequest, RequestOptions.DEFAULT );
    }

    @Override
    public Cancellable deleteAsync(String indexName, String id, T t, ActionListener<DeleteResponse> listener) {
        return deleteAsync ( indexName, id, null, null, t, listener );
    }

    @Override
    public Cancellable deleteAsync(String indexName, String id, T t, String routing, ActionListener<DeleteResponse> listener) {
        return deleteAsync ( indexName, id, routing, null, t, listener );
    }

    @Override
    public Cancellable deleteAsync(String indexName, String id, String pipeline, T t, ActionListener<DeleteResponse> listener) {
        return deleteAsync ( indexName, id, null, pipeline, t, listener );
    }

    @Override
    public Cancellable deleteAsync(String indexName, String id, String routing, String pipeline, T t, ActionListener<DeleteResponse> listener) {
        DeleteRequest request = esDeleteRequestImpl.getRequest ( indexName, id, routing, pipeline, t );
        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        return highLevelClient.deleteAsync ( request, RequestOptions.DEFAULT, listener );
    }

    @Override
    public Cancellable deleteBulkAsync(String indexName, List<String> ids, List<T> tList, ActionListener<BulkResponse> listener) {
        return deleteBulkAsync ( indexName, ids, null, null, tList, listener );
    }

    @Override
    public Cancellable deleteBulkAsync(String indexName, List<String> ids, List<T> tList, String routing, ActionListener<BulkResponse> listener) {
        return deleteBulkAsync ( indexName, ids, routing, null, tList, listener );
    }

    @Override
    public Cancellable deleteBulkAsync(String indexName, List<String> ids, String pipeline, List<T> tList, ActionListener<BulkResponse> listener) {
        return deleteBulkAsync ( indexName, ids, null, pipeline, tList, listener );
    }

    @Override
    public Cancellable deleteBulkAsync(String indexName, List<String> ids, String routing, String pipeline, List<T> tList, ActionListener<BulkResponse> listener) {
        BulkRequest bulkRequest = BulkRequestHelper.getBulkRequest ( indexName, ids, routing, pipeline, tList, esDeleteRequestImpl );
        RestHighLevelClient highLevelClient = esRestClient.getRestClient ();
        return highLevelClient.bulkAsync ( bulkRequest, RequestOptions.DEFAULT, listener );
    }

//TODO
    /** how to handle response
     *
     * String index = deleteResponse.getIndex();
     * String id = deleteResponse.getId();
     * long version = deleteResponse.getVersion();
     * ReplicationResponse.ShardInfo shardInfo = deleteResponse.getShardInfo();
     * if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
     *
     * }
     * if (shardInfo.getFailed() > 0) {
     *     for (ReplicationResponse.ShardInfo.Failure failure :
     *             shardInfo.getFailures()) {
     *         String reason = failure.reason();
     *     }
     * }
     */

}
