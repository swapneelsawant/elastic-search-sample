package com.ateam.elastic.rest;

import org.elasticsearch.action.support.replication.ReplicatedWriteRequest;

public interface ESRequestApi<T, R extends ReplicatedWriteRequest> {
    R getRequest(String indexName, String id, String routing, String pipeline, T t);
}
