package com.ateam.elastic.utils;

import com.ateam.elastic.exception.ESRuntimeException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;

public class JsonSerializer<T> {

    private ObjectMapper mapper;

    public JsonSerializer() {
        mapper = new ObjectMapper ();
        mapper.disable ( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS );
        mapper.setDateFormat ( new StdDateFormat ().withColonInTimeZone ( true ) );
    }

    public String toJson(T t) {
        try {
            return mapper.writeValueAsString ( t );
        } catch (JsonProcessingException e) {
            throw new ESRuntimeException ( e );
        }
    }

    public T fromJson(String json, Class zass) {
        try {
            return (T) mapper.readValue ( json, zass );
        } catch (JsonProcessingException e) {
            throw new ESRuntimeException ( e );
        }
    }
}
