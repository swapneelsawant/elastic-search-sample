package com.ateam.elastic.bo;

import java.util.Date;

public class Employee {

    private String name;
    private Date doj;
    private Address address;

    public Employee() {
    }

    public Employee(String name, Date doj, Address address) {
        this.name = name;
        this.doj = doj;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDoj() {
        return doj;
    }

    public void setDoj(Date doj) {
        this.doj = doj;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", doj=" + doj +
                ", address=" + address +
                '}';
    }
}

