package com.ateam.elastic.bo;

public class Address {

    private String building;
    private String city;

    public Address() {
    }

    public Address(String building, String city) {
        this.building = building;
        this.city = city;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{" +
                "building='" + building + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}