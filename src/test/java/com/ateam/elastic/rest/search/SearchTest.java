package com.ateam.elastic.rest.search;

import com.ateam.elastic.bo.Employee;
import com.ateam.elastic.rest.client.ESRestClient;
import com.ateam.elastic.rest.client.high.ESRestClientImpl;
import com.ateam.elastic.utils.JsonSerializer;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

public class SearchTest {

    private static ESRestClient<RestHighLevelClient> esRestClient;
    private static String INDEX_NAME = "employee";
    private static JsonSerializer<Employee> jsonSerializer = new JsonSerializer<> ();

    @BeforeClass
    public static void init() {
        esRestClient = new ESRestClientImpl ();
        esRestClient.initRestClient ();
    }

    @AfterClass
    public static void tearDown() throws IOException {
        esRestClient.shutdownRestClient ();
    }

    @Test
    public void search() throws IOException {
        ESSearchingApi esSearchingApi = new ESSearchingImpl ( esRestClient );

        BoolQueryBuilder boolQuerybuilder = new BoolQueryBuilder ();
        // boolQuerybuilder.must ( QueryBuilders.matchQuery ("name", "Daksh") );
        boolQuerybuilder.must ( QueryBuilders.matchQuery ( "address.building", "Pawan" ) );

        SearchResponse searchResponse = esSearchingApi.search ( INDEX_NAME, boolQuerybuilder );
        SearchHits hits = searchResponse.getHits ();

        SearchHit[] searchHits = hits.getHits ();

        Assert.assertEquals ( 1, searchResponse.getHits ().getHits ().length );

        for (SearchHit hit : searchHits) {
            String sourceAsString = hit.getSourceAsString ();
            Employee employee = jsonSerializer.fromJson ( sourceAsString, Employee.class );
            Assert.assertEquals ( "Daksh Sawant", employee.getName () );
        }


    }
}
