package com.ateam.elastic.rest.index;

import com.ateam.elastic.bo.Address;
import com.ateam.elastic.bo.Employee;
import com.ateam.elastic.rest.client.ESRestClient;
import com.ateam.elastic.rest.client.high.ESRestClientImpl;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;

public class TestIndexing {

    private static ESRestClient<RestHighLevelClient> esRestClient;
    private static String INDEX_NAME = "employee";

    @BeforeClass
    public static void init() {
        esRestClient = new ESRestClientImpl ();
        esRestClient.initRestClient ();
    }

    @AfterClass
    public static void tearDown() throws IOException {
        esRestClient.shutdownRestClient ();
    }

    public Employee getEmployee() {
        return new Employee ( "Daksh Sawant", new Date (), new Address ( "Pawan Apt", "Mumbai" ) );
    }

    @Test
    public void index() throws IOException {
        ESIndexingApi<Employee> esIndexingApi = new ESIndexingImpl<> ( esRestClient );
        IndexResponse indexResponse = esIndexingApi.index ( INDEX_NAME, "1", getEmployee () );
        RestStatus status = indexResponse.status ();
        Assert.assertEquals ( RestStatus.OK, status );
    }
}
